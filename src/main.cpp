#include <iostream>
#include <cstring>
#include <netinet/ip.h>
#include <unistd.h> //getopt
#include <dirent.h>
#include <sys/types.h>
#include <regex>

using namespace std;

#define DBG(...) printf("== sk == "); printf(__VA_ARGS__)

#define ROS1_DISTRO "melodic"
#define ROS2_DISTRO "eloquent"

int usage(void) {
    return 0;
}

int roscore_opened(int roscore_port)
{
    int addrlen, enable, epollfd;
    int listen_sock;
    struct sockaddr_in addr;

    listen_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    enable = 1;
    if (setsockopt(listen_sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
        printf("setsockopt(SO_REUSEADDR) failed");

    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port   = htons(roscore_port);
    addr.sin_addr.s_addr = INADDR_ANY;

    if (bind(listen_sock, (struct sockaddr *) &addr, sizeof(addr)) == -1)
    {
        printf("roscore was already opened on port 11311\n");
        fflush(stdout);
        return true;
    }
    close(listen_sock);
    return false;
}

int run_roscore(void)
{
    std::string roscore_cmd;
    roscore_cmd  = ".";
    roscore_cmd += " ";
    roscore_cmd += "/opt/ros/"ROS1_DISTRO"/setup.sh";
    // roscore_cmd += " >> /dev/null";
    roscore_cmd += " ";
    roscore_cmd += "&";
    roscore_cmd += "&";
    roscore_cmd += " ";
    roscore_cmd += "roscore";
    roscore_cmd += " ";
    roscore_cmd += "&";
    // ". /opt/ros/<ROS1_DISTRO>/setup.sh && roscore &"
    fprintf(stderr,"%s", roscore_cmd.c_str());
    return system(roscore_cmd.c_str());
}

int load_params(std::string path, std::string filename, std::string ns)
{
    while(!roscore_opened(11311));
    std::string load_cmd;
    load_cmd  = ".";
    load_cmd += " ";
    load_cmd += "/opt/ros/"ROS1_DISTRO"/setup.sh";
    // load_cmd += " >> /dev/null";
    load_cmd += " ";
    load_cmd += "&";
    load_cmd += "&";
    load_cmd += " ";
    load_cmd += "rosparam load "+ path + "/" + filename +" "+ ns;
    // ". /opt/ros/<ROS1_DISTRO>/setup.sh && rosparam load <filename>  <namespace>"
    fprintf(stderr,"%s", load_cmd.c_str());
    return system(load_cmd.c_str());
}

int run_bridge(std::string ns)
{
    std::string bridge_cmd;
    bridge_cmd  = ".";
    bridge_cmd += " ";
    bridge_cmd += "/opt/ros/"ROS1_DISTRO"/setup.sh";
    // bridge_cmd += " >> /dev/null";
    bridge_cmd += " ";
    bridge_cmd += "&";
    bridge_cmd += "&";
    bridge_cmd += " ";
    bridge_cmd += ".";
    bridge_cmd += " ";
    bridge_cmd += "/opt/ros/"ROS2_DISTRO"/setup.sh";
    // bridge_cmd += " >> /dev/null";
    bridge_cmd += " ";
    bridge_cmd += "&";
    bridge_cmd += "&";
    bridge_cmd += " ";
    bridge_cmd += "ros2";
    bridge_cmd += " ";
    bridge_cmd += "run";
    bridge_cmd += " ";
    bridge_cmd += "ros1_bridge";
    bridge_cmd += " ";
    bridge_cmd += "parameter_bridge";
    bridge_cmd += string(" ")+"_"+"_"+"n"+"s"+":"+"="+ ns;
    bridge_cmd += " ";
    bridge_cmd += "&";
    fprintf(stderr,"%s", bridge_cmd.c_str());
    return system(bridge_cmd.c_str());
}

int run_all_bridges(std::string path)
{
    DIR *dir_ptr;
    struct dirent *direntp;
    dir_ptr = opendir(path.c_str());

    if(dir_ptr == NULL)
    {
        fprintf(stderr,"Can not open %s", path.c_str());
    }
    else 
    {
        while((direntp = readdir(dir_ptr)) != NULL) 
        {
            std::string filename(direntp->d_name);
            std::regex e("^.*\.[yY][aA][mM][lL]$");
            if(std::regex_match(filename, e)) 
            {
                std::string ns = 
                    filename.substr(0, filename.find_last_of("."));
                printf("%s\n", ns.c_str());
                load_params(path, filename, ns);
                run_bridge(ns);
            }
        }
        closedir(dir_ptr);
    }
    return 0;
}

int run_docker(void)
{
    return 0;
}

int main(int argc, char *argv[])
{
    int opt = 0;
    system("echo $ROS_MASTER_URI");

    opterr = 1; // Enable print opterr msg
#if 1
    if(!roscore_opened(11311)){
        run_roscore();
    }
#endif
    system("echo $ROS_MASTER_URI");

    while(1) {

        opt = getopt(argc, argv,"dp::s:");

        if(opt == -1)
            break;

        switch(opt) {
            case 'd':
                printf("ddd %s", optarg);
                fflush(stdout);
                break;
            case 'p':
                printf("===", optarg);
                fflush(stdout);
                if(optarg) {
                    std::string path(optarg);
                    run_all_bridges(path);
                }
                break;
            case 's':
                printf("%s", optarg);
                fflush(stdout);
                break;
            default: // '?'
                DBG("TEST\n");
                usage();
                return 0;
        }
    }
    //Do we have args?
    if (argc > optind) {
        int i = 0;
        for (i = optind; i < argc; i++) {
            fprintf(stderr, "argv[%d] = %s\n", i, argv[i]);
        }
    }
    while(1);
    system("kill 0 -9");
    system("killall /opt/ros/dashing/lib/ros1_bridge/parameter_bridge -9");
    return 0;
}
